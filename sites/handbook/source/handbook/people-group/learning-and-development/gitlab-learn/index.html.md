---
layout: handbook-page-toc
title: GitLab Learn
description: “Discover GitLab Learn, a Learning Experience Platform for the GitLab community”
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Learn

## What is EdCast?

GitLab uses EdCast, a Learning Experience Platform (LXP). Our EdCast instance is named [GitLab Learn](https://gitlab.edcast.com/). At GitLab, we believe [everyone can contribute](https://about.gitlab.com/company/mission/#mission), and our LXP is no different. [Everyone can contribute to GitLab Learn!](/handbook/people-group/learning-and-development/gitlab-learn/contribute/) 

You can read more about the mission and vision of the LXP on the [EdCast About Page](https://www.edcast.com/corp/about-us/).

Users of GitLab Learn can find support in our [user docs](/handbook/people-group/learning-and-development/gitlab-learn/user/). 

Administrators can find best practices and workflows for creating and managing content in our [admin docs](/handbook/people-group/learning-and-development/gitlab-learn/admin/).

### What is a LXP?

A learning experience platform, or LXP, is [defined by EdCast](https://www.edcast.com/corp/blog/what-is-an-lxp/#:~:text=It%20brings%20together%20internal%2C%20external,the%20content%20to%20the%20user.) as a platform that "brings together internal, external, formal and informal sources of learning and knowledge in a simple, easy to use interface (UI), driving an intuitive user experience (UX).  A key feature is that an LXP should use Artificial Intelligence and Machine Learning (AI/ML) to personalize the content to the user. An LXP should be available on the web, and on a mobile device, anytime, anywhere."

## GitLab's Long Term Strategic Vision for the LXP

Our long term goal for [GitLab Learn](https://about.gitlab.com/learn/) is to create a centralized learning hub for all learning material produced by GitLab. This includes:

1. Training for team members that is available to the wider community
1. Internal training for team members
1. Professional services training for customers and partners
1. Learning material for GitLab program members, like the GitLab for Education program
1. Technical marketing and education content

All learning handbook follows our [handbook first learning best practices](https://about.gitlab.com/handbook/people-group/learning-and-development/interactive-learning/) and aligns with our [GitLab CREDIT values.](https://about.gitlab.com/handbook/values/). The LXP is [team member](/handbook/people-group/learning-and-development/gitlab-learn/contribute/) and community driven with content on an open platform for all to consume! 

### Brandon Hall Award

The launch of the GitLab Learn LXP was awarded a Silver Excellence in Learning by the [Brandon Hall Group](http://www.brandonhall.com/excellenceawards/) for the Best Launch of a Corporate Learning University. 

![silver brandon hall group award logo](silver-award.png){: .shadow.medium.center}

#### A note about naming

Review the following terms to get familiar with language used in this documentation.

| Term | Definition |
| ----- | ----- |
| EdCast | The vendor we're collaborating with to create GitLab Learn. |
| GitLab Learn | The GitLab EdCast instance |
| Learning Experience Platform (LXP) | The type of learning platform that GitLab is using to organize learning content. Learn more in the [L&D handbook](https://about.gitlab.com/handbook/people-group/learning-and-development/#gitlab-learn-edcast-learning-experience-platform-lxp) |


## What will GitLab Learn do for the GitLab community?

GitLab has invested in the LXP to enable our team members, partners, customers, and the broader community a personalized, self-service learning platform to enable handbook- first continuous learning. GitLab Learn will do the following for GitLab: 

- **Aggregate** all knowledge and learning content into one system for easier accessibilitiy 
- **Curate & Create** content that is [handbook first with interactivity](/handbook/people-group/learning-and-development/interactive-learning/)
- **Recommend** learning based on individual community member preferences
- **Learn & Guide** community members on how to develop new skills through step-by-step guides through individual learning styles
- **Automate** learning for personalized learning recommendations
- **Scale** learning to the wider GitLab community
- **On-Demand**, **Self-Paced** learning
- **Reduce** manual tasks associated with scaling
- **Enhance** training to be delivered in a fun and engaging way (erase your preconceived notions of LMS or LXP systems!)
- **Dogfood** the same LXP system that will be used for customer training offerings

### Why should GitLab teams add professional development material to GitLab Learn?

If your team has created learning paths or curated external resources for team member professional development, we highly suggest adding this content to the LXP. 

By using the LXP to administer learning, your team can:

1. Track completion and engagement of learning material
1. Share earned badges and certifications externally on your LinkedIn profile
1. Provide interactive learning modalities that feature handbook first material that can be applied to various learning styles
1. Develop balanced curated and customized learning content that addresses functional competency gaps 
1. Manage all of your learning content in one place with a focus on peer learning
1. Suggest related training based on previously completed or viewed content using machine learning in the EdCast platform
1. Enable other team members and community members to access learning content

If you're a GitLab team member looking to collaborate with the Learning and Development team in adding content to GitLab Learn, please review the process on our [work with us handbook page](/handbook/people-group/learning-and-development/work-with-us).


## Handbook first approach to the LXP

The GitLab LXP uses a [handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first) approach for all learning content, using the handbook as our single source of truth for learning content. Contributors to the LXP will use the platform to [find and curate](/handbook/people-group/learning-and-development/interactive-learning/) relevant content that exists in the handbook using a structured process. 

## Governance

[Implementation of the EdCast LXP](/handbook/people-group/learning-and-development/gitlab-learn/implementation) at GitLab requires cross-functional collaboration across organizations to serve various audience needs. In the spirit of driving clarity, visibility, and accountability, we've identified DRIs for the roles and responsibilities outlined below.

Overall LXP Implementation DRI: Josh Zimmerman Learning & Development

### Steering Committee

| Name | Team | Executive Sponsor |
| ------ | ------ | ------ |
| David Somers | Field Enablement | David Hong |
| Kendra Marquart | Professional Services | Michael Lutz/David Sakamoto |
| Josh Zimmerman | L&D | Wendy Barnes |
| David Somers | Channel/Partner Enablement | Michelle Hodges | 
| Tye David | Marketing | Harsh Jawharkar | 

### Audience Workstreams

- GitLab Team Members
- Partners
- Customers
- Community Members

### LXP Project DRIs

| Gap | Description | DRI |
| ------ | ------ | ------ |
| Exec alignment | Need to be 100% aligned with LXP vision with Sid and e-group | David Somers |
| Content QA - Handbook Alignment | Responsible for ensuring that all content on the LXP is handbook first, especially since content is regularly updated in the handbook and with the use of Articulate 360 (third party tool)  | Samantha Lee |
| Content look and feel | Ensuring that all content has a similar look and feel, unified across Field Enablement, Partner Enablement, Community, Marketing, L&D, etc. | Samantha Lee |
| Legal | Alignment with legal on EdCast features and capabilities for compliance of course content | Robert Nalen |
| Branding | Need to have an EdCast designer that develops the look and feel  that is the same for partners, customers, and team members. Need logo, name, SEO, landing page, card styles, css style files, etc. | Samantha Lee, collaborating with Tye Davis |
| Marketing | Coordination with marketing team on larger marketing strategy and tactics (i.e. do we have campaigns, where is the landing page, what is the SEO, how does this fit into pipeline, third party content providers (Coursera)) | Samantha Lee  |
| Manager Functionality & Reporting | Need to determine how managers can monitor team member learning paths and dashboard of training completion | Learning and Development Team |
| System Design | Should the owner of systems settings be organized at the highest level, not by department? | Learning and Development Team |
| Content Strategy | Need to determine what the content categories are across the organization | TBD |
| Content Management | Will content be managed by respective departments or will this be done a centralized level? | Learning and Development team |
| Assessments | Standardization of assessment look and feel across the organization | Related content owner |
| Notifications | Determine how customers, partners, and team members will receive notifications and who owns the notification process to respective parties | Learning and Development team |
| Support | Determine how technical issues and questions about the LXP will be managed | Learning and Development and Professional Services teams |
| `www.about.gitlab.com/learn` Alignment | Determine how the LXP will be linked with the [GitLab Learn landing page](https://gitlab.com/groups/gitlab-com/marketing/-/epics/954#note_429575616) | Tye Davis |


### Core Team Members and Roles

| Name | Team at GitLab |
| ----- | ----- |
| Christopher Nelson | IT |
| Josh Zimmerman | Learning and Development |
| Jacie Bandur | Learning and Development |
| Samantha Lee | Learning and Development |
| David Somers | Field Enablement |
| John Blevins | Field Enablement |
| Kris Reynolds | Field Enablement |
| Monica Jacobs | Field Enablement |
| Tanuja Paruchuri | Field Enablement |
| Kelley Shirazi | Field Enablement |
| Pallavi Daliparthi | Field Enablement |
| Thabo Bopape | Field Enablement |
| Emelie Rodriguez | Field Enablement |
| Christina Hupy | Marketing Enablement |
| Christopher Wang | Marketing Enablement |
| Christine Yoshida | Professional Services |
| Kendra Marquart | Professional Services |
| Wakae McLaurin | Professional Services |
| Ed Cepulis | Channel Enablement |
| Kim Jaeger | Channel Enablement |
| Honora Duncan | Channel Enablement |
| Evon Collett | Channel Enablement |
| Boughty Canton | Channel Enablement |
| Tye Davis | Technical Marketing |
| Julia Lake | Security/Legal |
| Lynsey Sayers | Security/Legal |


## Platform Roles and Permissions

The EdCast platform enables creating of custom user roles. This section outlines our roles, identifies the permissions of each role, and provides clarity on the audience that has access to or can request these roles.

The following permissions are available on the platform and can be set to the `on` or `off` status for each role:

1. Create comment
1. Like content
1. Bookmark content
1. Mark as complete
1. Assign content
1. Dismiss content
1. Dismiss assignment
1. Add to pathway
1. Create group
1. Create group admin
1. Create channel
1. Disable user follow
1. Can rate
1. Share
1. Show user manager
1. Show content manager
1. Assign roles
1. Create content
1. Create text card
1. Promote a card
1. Create a journey
1. Create podcast
1. Curate content
1. Create role
1. Create leap
1. Update leap
1. Delete leap
1. Pin content
1. View card analytics
1. Get leaderboard information
1. User opt out of leaderboard
1. Create Livestream/AMA
1. Upload
1. Upload content cover images
1. Enable edgraph
1. Custom labels
1. Publish links
1. Change author
1. Bypass curation
1. Developer
1. Upload scorm content
1. Use dynamic selection
1. Perfect your pitch
1. Copy content
1. Mark as private
1. Manage content
1. Manage group content
1. Manage channels
1. Manage users
1. Manage group users
1. Manage analytics
1. Manage channel analytics
1. Manage group analytics

## How to request elevated role permissions in EdCast

Follow the steps outlined in the handbook to open an [individual access request for the desired elevated role in EdCast](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/) and assign the AR to `@slee24`.


### Platform Administrator

#### Permissions

| Permission | Status |
| ----- | ----- |
| All Permissions | On |

#### Platform Administrator user group

The `Platform Administrator` role is reserved for team members involved in overall platform management and organization. The Learning and Development team manages the GitLab Learn instance. This entire team should have `Platform Administrator` rights. 

In addition, the Professional Services team uses the platform extensively for paid and unpaid certifications, so some PS team members might require `Platform Administrator` access. Others heavily involved in the building and maintence of GitLab Learn can request this access level. 

| Team Member Name | Team |
| ----- | ----- |
| Josh Zimmerman | Learning and Development |
| Jacie Bandur | Learning and Development |
| Samantha Lee | Learning and Development |
| Christine Yoshida | Professional Services |
| Kendra Marquart | Professional Services |
| Wakae McLaurin | Professional Services

#### Task examples

| Platform Administrator Responsibility | DRI |
| ----- | ----- |
| LinkedIn Learning integration and licenses | Jacie Bandur |
| Discover page organization. content promotion, and custom carousel creation | Samantha Lee |
| Platform login permissions | Samantha Lee |
| Notification settings | Samantha Lee |
| Paid certifications | Christine Yoshida |

#### Requesting support from Platform Administrators

Content Adminitrators and Learning Evangelists might request support from the `Platform Administrators` for platform updates and content promotion. Communication should be done in Slack via the #learninganddevelopment channel. Example scenarios where `Platform Administrators` might need to be involved include:

1. Content to be featured on Discover Page
1. A new learning group created for a GitLab Team
1. LinkedIn Learning access and course recommendations
1. Enrollment in a paid certification

### Content Administrator

#### Permissions

| Permission | Status |
| ----- | ----- |
| Create comment | On |
| Like content | On |
| Bookmark content | On |
| Mark as complete | On |
| Assign content | On |
| Dismiss content | On |
| Dismiss assignment | On |
| Add to pathway | On |
| Create group | On |
| Create group admin | On |
| Create channel | On |
| Disable user follow | On |
| Can rate | On |
| Share | On |
| Show user manager | On |
| Show content manager | On |
| Assign roles | Off |
| Create content | On |
| Create text card | On |
| Promote a card | On |
| Create a journey | On |
| Create podcast | On |
| Curate content | On |
| Create role | Off |
| Create leap | On |
| Update leap | On |
| Delete leap | On |
| Pin content | On |
| View card analytics | On |
| Get leaderboard information | On |
| User opt out of leaderboard | On |
| Create Livestream/AMA | On |
| Upload | On |
| Upload content cover images | On |
| Enable edgraph | On |
| Custom labels | On |
| Publish links | On |
| Change author | On |
| Bypass curation | On |
| Developer | Off |
| Upload scorm content | On |
| Use dynamic selection | On |
| Perfect your pitch | On |
| Copy content | On |
| Mark as private | On |
| Manage content | On |
| Manage group content | On |
| Manage channels | On |
| Manage users | Off |
| Manage group users | On |
| Manage analytics | On |
| Manage channel analytics | On |
| Manage group analytics | On |

#### Content Administrator user group

`Content administrators` are granted permissions to follow most processes outline in the [GitLab Learn admin handbook](/handbook/people-group/learning-and-development/gitlab-learn/admin). This admin group should feel confident in doing these admin tasks without needing to gain approval from other content or platform administrators. If there is a change made that might impact users in another group or team, the admin should gain approval via Slack in the #learninganddevelopment channel.

| Team Member Name | Team |
| ----- | ----- |
| Kelley Shirazi | Field Enablement |
| John Blevins | Field Enablement |

#### Task examples

| Category | Approved Tasks |
| ----- | ----- |
| Content Upload - SmartCards, Pathways, and Journeys | Upload/Edit/Manage learning content based on platform best practices, Share content with Groups and Channels |
| Groups | Create/Edit/Manage Groups, Set up Dynamic Group Workflows |
| Channels | Create/Edit/Manage Channels |


### Learning Evangelist

#### Permissions

| Permission | Status |
| ----- | ----- |
| Create comment | Off |
| Like content | Off |
| Bookmark content | Off |
| Mark as complete | Off |
| Assign content | On |
| Dismiss content | Off |
| Dismiss assignment | Off |
| Add to pathway | On |
| Create group | Off |
| Create group admin | Off |
| Create channel | Off |
| Disable user follow | On|
| Can rate | Off |
| Share | Off |
| Show user manager | Off |
| Show content manager | Off |
| Assign roles | Off |
| Create content | On |
| Create text card | On |
| Promote a card | On |
| Create a journey | On |
| Create podcast | On |
| Curate content | On |
| Create role | Off |
| Create leap | On |
| Update leap | On |
| Delete leap | On |
| Pin content | On |
| View card analytics | Off |
| Get leaderboard information | On |
| User opt out of leaderboard | Off |
| Create Livestream/AMA | Off |
| Upload | On |
| Upload content cover images | On |
| Enable edgraph | Off |
| Custom labels | On |
| Publish links | On |
| Change author | On |
| Bypass curation | On |
| Developer | Off |
| Upload scorm content | On |
| Use dynamic selection | On |
| Perfect your pitch | Off |
| Copy content | Off |
| Mark as private | On |
| Manage content | Off |
| Manage group content | Off |
| Manage channels | Off |
| Manage users | Off |
| Manage group users | Off |
| Manage analytics | Off |
| Manage channel analytics | Off |
| Manage group analytics | Off |

#### Learning Evangelist user group

The `Learning Evangelist` role is reserved for team members who have successfully completed the [Learning Evangelist Pathway](/handbook/people-group/learning-and-development/gitlab-learn/contribute/team-member-contributions/) in GitLab Learn. This Pathway trains users on how to contribute branded, handbook-first content to the platform. This role is only granted to users who also have the `GitLab Team Members` role enabled.

#### Task examples

- A GitLab team member contributes an interesting article they read about remote leadership
- A manager uploads a learning pathway to train their team on a new skill or tool


### Curator

#### Permissions

| Permission | Status |
| ----- | ----- |
| Create comment | Off |
| Like content | Off |
| Bookmark content | Off |
| Mark as complete | Off |
| Assign content | Off |
| Dismiss content | Off |
| Dismiss assignment | Off |
| Add to pathway | On |
| Create group | Off |
| Create group admin | Off |
| Create channel | Off |
| Disable user follow | Off|
| Can rate | Off |
| Share | Off |
| Show user manager | Off |
| Show content manager | Off |
| Assign roles | Off |
| Create content | Off |
| Create text card | Off |
| Promote a card | Off |
| Create a journey | On |
| Create podcast | Off |
| Curate content | On |
| Create role | Off |
| Create leap | On |
| Update leap | On |
| Delete leap | On |
| Pin content | Off |
| View card analytics | Off |
| Get leaderboard information | Off |
| User opt out of leaderboard | Off |
| Upload | Off |
| Upload content cover images | Off |
| Enable edgraph | Off |
| Custom labels | On |
| Publish links | Off |
| Change author | Off |
| Bypass curation | Off |
| Developer | Off |
| Upload scorm content | Off |
| Use dynamic selection | Off |
| Perfect your pitch | Off |
| Copy content | Off |
| Mark as private | On |
| Manage content | Off |
| Manage group content | Off |
| Manage channels | Off |
| Manage users | Off |
| Manage group users | Off |
| Manage analytics | Off |
| Manage channel analytics | Off |
| Manage group analytics | Off |

#### Curator user group

The `Curator` role is for GitLab team members who are intersted in curating existing content on GitLab Learn for their team, speifically content from LinkedIn Learning. This role can be useful for teams who want to suggest LinkedIn Learning content for their team, but do not need to create or contribute new learning content like a Learning Evangelist. This role is only granted to users who also have the `GitLab Team Members` role enabled.


### Member

#### Permissions

| Permission | Status |
| ----- | ----- |
| Create comment | On |
| Like content | On |
| Bookmark content | On |
| Mark as complete | On |
| Assign content | Off |
| Dismiss content | On |
| Dismiss assignment | Off |
| Add to pathway | Off |
| Create group | Off |
| Create group admin | Off |
| Create channel | Off |
| Disable user follow | On |
| Can rate | On |
| Share | On |
| Show user manager | Off |
| Show content manager | Off |
| Assign roles | Off |
| Create content | Off |
| Create text card | Off |
| Promote a card | Off |
| Create a journey | Off |
| Create podcast | Off |
| Curate content | Off |
| Create role | Off |
| Create leap | Off |
| Update leap | Off |
| Delete leap | Off |
| Pin content | Off |
| View card analytics | Off |
| Get leaderboard information | On |
| User opt out of leaderboard | On |
| Upload | Off |
| Upload content cover images | Off |
| Enable edgraph | Off |
| Custom labels | Off |
| Publish links | Off |
| Change author | Off |
| Bypass curation | Off |
| Developer | Off |
| Upload scorm content | Off |
| Use dynamic selection | Off |
| Perfect your pitch | Off |
| Copy content | Off |
| Mark as private | Off |
| Manage content | Off |
| Manage group content | Off |
| Manage channels | Off |
| Manage users | Off |
| Manage group users | Off |
| Manage analytics | Off |
| Manage channel analytics | Off |
| Manage group analytics | Off |

#### Member user group

The `Member` role is assigned to all platform users automatically upon signing up to the platfomr. This includes GitLab team members, customers, and community members.


### EdGraph: Reporting Access

The `EdGraph: Reporting Access` role allows users to view data analtyics and reports in the EdGraph platform. This role will enable EdGraph to appear in the user's waffle menu via the EdCast platform.

#### Permissions

| Permissions | Status |
| ----- | ----- |
| Enable EdGraph | On |
| All other permissions | Off |

#### EdGraph: Reporting Access user group

Our team is working on iterating analytics management from GitLab Learn to be visible in Sisense. Right now, the implementation team has this role provisioned on their accounts. However, it will be deprecated or restricted to Platform Administrators when the transition to Sisense is complete.

#### Task examples

- View broad analytics of the platform, including engaged users, course completions, etc.
- Build and pull custom reports on specific SmartCards, Pathways, and Journeys


### Manager

#### Permissions

| Permission | Status |
| ----- | ----- |
| Create comment | Off |
| Like content | Off |
| Bookmark content | Off |
| Mark as complete | Off |
| Assign content | On |
| Dismiss content | Off |
| Dismiss assignment | Off |
| Add to pathway | Off |
| Create group | Off |
| Create group admin | Off |
| Create channel | Off |
| Disable user follow | Off |
| Can rate | Off |
| Share | On |
| Show user manager | On |
| Show content manager | On |
| Assign roles | Off |
| Create content | Off |
| Create text card | Off |
| Promote a card | Off |
| Create a journey | Off |
| Create podcast | Off |
| Curate content | Off |
| Create role | Off |
| Create leap | Off |
| Update leap | Off |
| Delete leap | Off |
| Pin content | Off |
| View card analytics | Off |
| Get leaderboard information | Off |
| User opt out of leaderboard | Off |
| Upload | Off |
| Upload content cover images | Off |
| Enable edgraph | Off |
| Custom labels | Off |
| Publish links | Off |
| Change author | Off |
| Bypass curation | Off |
| Developer | Off |
| Upload scorm content | Off |
| Use dynamic selection | Off |
| Perfect your pitch | Off |
| Copy content | Off |
| Mark as private | Off |
| Manage content | Off |
| Manage group content | Off |
| Manage channels | Off |
| Manage users | Off |
| Manage group users | Off |
| Manage analytics | Off |
| Manage channel analytics | Off |
| Manage group analytics | Off |

#### Manager user group

The `Manager` role is reserved for GitLab Managers who are interested in assigning content to their team in GitLab Learn. This role is only granted to users who also have the `GitLab Team Members` role enabled.

#### Task examples

- Assign learning to direct reports

### Group_Admin

The `Group_Admin` role is assigned to users who manage a Group in EdCast.

#### Permissions

| Permission | Status |
| ----- | ----- |
| Create comment | Off |
| Like content | Off |
| Bookmark content | Off |
| Mark as complete | Off |
| Assign content | Off |
| Dismiss content | Off |
| Dismiss assignment | Off |
| Add to pathway | Off |
| Create group | On |
| Create group admin | On |
| Create channel | Off |
| Disable user follow | Off |
| Can rate | Off |
| Share | Off |
| Show user manager | On |
| Show content manager | Off |
| Assign roles | Off |
| Create content | Off |
| Create text card | Off |
| Promote a card | Off |
| Create a journey | Off |
| Create podcast | Off |
| Curate content | Off |
| Create role | Off |
| Create leap | Off |
| Update leap | Off |
| Delete leap | Off |
| Pin content | Off |
| View card analytics | Off |
| Get leaderboard information | Off |
| User opt out of leaderboard | Off |
| Upload | Off |
| Upload content cover images | Off |
| Enable edgraph | Off |
| Custom labels | Off |
| Publish links | Off |
| Change author | Off |
| Bypass curation | Off |
| Developer | Off |
| Upload scorm content | Off |
| Use dynamic selection | Off |
| Perfect your pitch | Off |
| Copy content | Off |
| Mark as private | Off |
| Manage content | Off |
| Manage group content | On |
| Manage channels | Off |
| Manage users | Off |
| Manage group users | On |
| Manage analytics | Off |
| Manage channel analytics | Off |
| Manage group analytics | On |

### Group_Leader

The `Group_Admin` role is assigned to users who support the `Group_Admin` by contributing to group curation. This role is often paired with the `Curator` or `Learning Evangelist` role.

#### Permissions

| Permission | Status |
| ----- | ----- |
| Create comment | Off |
| Like content | Off |
| Bookmark content | Off |
| Mark as complete | Off |
| Assign content | Off |
| Dismiss content | Off |
| Dismiss assignment | Off |
| Add to pathway | Off |
| Create group | Off |
| Create group admin | Off |
| Create channel | Off |
| Disable user follow | Off |
| Can rate | Off |
| Share | Off |
| Show user manager | Off |
| Show content manager | Off |
| Assign roles | Off |
| Create content | Off |
| Create text card | Off |
| Promote a card | Off |
| Create a journey | Off |
| Create podcast | Off |
| Curate content | Off |
| Create role | Off |
| Create leap | Off |
| Update leap | Off |
| Delete leap | Off |
| Pin content | Off |
| View card analytics | Off |
| Get leaderboard information | Off |
| User opt out of leaderboard | Off |
| Upload | Off |
| Upload content cover images | Off |
| Enable edgraph | Off |
| Custom labels | Off |
| Publish links | Off |
| Change author | Off |
| Bypass curation | Off |
| Developer | Off |
| Upload scorm content | Off |
| Use dynamic selection | Off |
| Perfect your pitch | Off |
| Copy content | Off |
| Mark as private | Off |
| Manage content | Off |
| Manage group content | On |
| Manage channels | Off |
| Manage users | Off |
| Manage group users | Off |
| Manage analytics | Off |
| Manage channel analytics | Off |
| Manage group analytics | Off |

### Custom Role: Partner

The `Custom Role: Partner` role is not yet defined and will be revisited upon phase 3 of the EdCast launch to include GitLab Partners.

### Custom Role: Customer

The `Custom Role: Customer` role is not yet defined and will be revisited upon phase 2 of the EdCast launch to include GitLab Customers.
