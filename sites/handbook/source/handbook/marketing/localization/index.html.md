---
layout: handbook-page-toc
title: Localization
description: Handbook page for localization processes.
---

## Introduction

This page contains guidelines for localization at GitLab. This page is maintained by the Global Content team.

## Overview

GitLab is a global company that does business around the world. In order to best serve our global customers, we localize our content and campaigns into native languages.

## Current State

Our current localization capablities are limited due to resources and bandwidth. Currently, we are focused on translating content that aligns to our P0 countries. Additionally, we are enabling our field marketing teams to get lighweight translations that don't require internal development or design resources.

### Internal Reviews

To ensure translated content makes contextual and technical sense, we have internal native language speakers review the content. At the moment, our internal reviewers are volunteers. We completely appreciate that this task is on top of your day job and will continue to respect that. Below are some expectations, as we will sometimes have hard deadlines to try to hit.

Internal reviewers **are expected** to review content for accuracy and quality and identify small mistakes made by the translator. Internal reviewers **are not expected** to copyedit or rewrite documents. However, reviewers may want provide preferred translations during the review process and they are welcome to do so. If the quailty of a translated document is poor and needs to be written, the reviewer should stop reading and let the requestor know that the translation is not usable and provide a reason why. The requestor should provide this feedback to the translator.

**Guidelines for job requesters**

1. State any specific deadlines or expected SLAs in the review request issue.
1. Stay in communication with internal reviewer about expectations and deadlines.

**Guidelines for reviewers**

1. Identify copy mistakes.
1. Identify areas where the context did not translate well and needs more attention.
1. Identify words or phrases that should **not** be translated.
1. If by the second page it is clear that the translation is too poor to be used, stop reading and let the requestor know that translation is not usable and should be resumbitted.

**Timeframe guideline for reviewers**

1. Stay in communication with the job requester - if you will be delayed reviewing a job please make sure to update the job requester. 
2. If you are tagged in a review issue along with others, please respond ASAP with if you have the bandwidth to take on the job. 
3. If you have claimed a job in Smartling, please complete the review in 5 - 7 business days. Delays are fine, as mentioned above, just stay in communication with the job requester. 
4. If you have trouble with Smartling, please put your question/issue into the slack channel: `#mktgops` and tag the job requester. 

#### Process for requesting a review

1. Open a [translation review request](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#)
1. Complete all requested fields in the issue.
1. Content DRI to assign the reviewer. Reviewers are rotated in the order listed below.

| Language | Reviewers |
| -------- | --------- |
| French | @k33g |
| German | @amimmo |

#### Process for requesting a review via Smartling

For some languages, a review step is built within the translation process in [Smartling](/handbook/marketing/marketing-operations/smartling). We have internal reviewers that are trained on Smartling to review and send back to the translators if there are any issues. 

Languages that the Smartling step is enabled for:

1. German
2. Russian
3. Japanese
4. Korean

Internal reviewers for these languages that will review via Smartling:

| Language | Reviewers |
| -------- | --------- |
| German | @KSetschin, @lmertensmeyer |
| Russian | @KSetschin, @V_Rusin, @dzalbo, @igor.drozdov |
| Japanese | @toshitakaito, @emorita, @yuki_murakami, @Tea_Tatsuki, @mueda1 |
| Korean | @taehohyun, @iyoo |
| French | @pgascouvaillancourt, @sahbabou |
| Portuguese (Brazilian) | @lvieiradossantos, @ricardoamarilla |
| Spanish (LATAM) | @lisvinueza, @jaime, @ricardoamarilla |
| Italian | @agulina |

Please follow the below process for requesting a review but please do so at the same time of opening the Smartling job:

1. Submit job in Smartling
2. Directly after, create a review issue and tag all the reviewers of language.
3. Reviewers have 24-48 hours to claim the job.
4. If it’s not claimed, the job requester has right to assign the job to one of the reviewers on the Gitlab issue.
5. Reviewer needs to claim the job on Smartling. Once it’s ready to review, they will get an email notification alert them that their job is ready to review
6. Once reviewed, if ready to publish, please publish the job and update on the Gitlab review issue to let the job requester know - it would be great if they could also let them know the quality of the translation.

### Priority Countries

Annually we prioritize countries that we believe offer the most opportunity for GitLab. A listing of those countries and our expected level of investment are tracked in an internal document called: [Priority Country Analysis](https://docs.google.com/spreadsheets/d/1eRrtRPdNSQjtvDrEvPJ_klfqKAatnLIzDjvShXhnSr8/edit?usp=sharing). 

Our current localization capabilities are limited due to resources and bandwidth. Currently, we are focused on translating content that aligns to our integrated campaign strategy. Additionally, we are enabling our field marketing teams to get lightweight translations that don't require internal development or design resources.

### Tools & Capabilities

| Vendor | Capabilities | Access |
| ------ | ------------ | ------ |
| Rev | English language transcriptions, foreign language subtitles | Open an access request issue |
| [Smartling](/handbook/marketing/marketing-operations/smartling) | Localization platform | Open an access request issue |

### Translating content for campaigns

Localized campaigns currently follow the the [integrated campaign process](/handbook/marketing/campaigns/#campaign-planning). The Content Marketing team is responsible for content localization for integrated campaigns. Content marketers are responsible for campaigns that fall within their subject area domain:

| Use Case | DRI |
| -------- | --- |
| VCC | Suri Patel |
| CI | TBD|
| DevSecOps | TBD |
| Simplify DevOps/DevOps platform | TBD |
| GitOps | Suri Patel |
| CD | TBD |
| Agile | Suri Patel |
| Cloud Native | TBD |

After a content asset or webpage has been translated, be sure to add it to the [Localization Resources](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit?usp=sharing) spreadsheet. If edit access, isn't available, send the content addition to @bmatturro to update.

### Language preference 

The following language preference segmentations are available in Marketo:
1. [French](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/SL52950653A1)
2. [German](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/SL52950651A1)
3. [Japanese](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/SL52950655A1)
4. [Korean](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/SL52953988A1)
5. [Default (English)](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/SL52950659A1)

### Translated URL structure

All translated pages live in a sub-folder dedicated to a specific language. These sub-folders use [ISO 639-1 codes] (https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes). For example, German pages live in the `/de/` sub-folder.

### hreflang tagging

Search engines use the `hreflang` tag to determine a canonical version for translated pages. We'll use `hreflang` on our translated pages.

`hreflang` tags start with declaration `<link rel="alternate"`, adds URL `href={{url}}`, and ends with `hreflang={{language ISO}}`

Example of a hreflang tag for a URL translated to German.

`<link rel="alternate" href="https://about.gitlab.com/de/warum/nutze-continuous-integration-fuer-schnelleres-bauen-und-testen/" hreflang="de" />`

The canonical version of our site will the United States English version on `about.gitlab.com`. We need to add all versions of a page under the page title and link to each one with the appropriate language noted. [Google provides this example](https://developers.google.com/search/docs/advanced/crawling/localized-versions?visit_id=637504000817145606-3833240924&rd=1):

```
<head>
 <title>Widgets, Inc</title>
  <link rel="alternate" hreflang="en-gb"
       href="http://en-gb.example.com/page.html" />
  <link rel="alternate" hreflang="en-us"
       href="http://en-us.example.com/page.html" />
  <link rel="alternate" hreflang="en"
       href="http://en.example.com/page.html" />
  <link rel="alternate" hreflang="de"
       href="http://de.example.com/page.html" />
 <link rel="alternate" hreflang="x-default"
       href="http://www.example.com/" />
</head>
```

It's important to note we need to declare the default page from our repository as the canonical version to avoid penalities across Google properties.

Aleyda Solis maintains a great [tool to build `hreflang` tags](https://www.aleydasolis.com/english/international-seo-tools/hreflang-tags-generator/) we can use for reference as well.

### List of Localized Websites

| Language | Localized Website | Status |
| ------ | ------------ | ------ |
| French | https://about.gitlab.com/fr-fr/ | Live |
| Japanese | https://about.gitlab.com/ja-jp/ | Live |
| German | https://about.gitlab.com/de-de/ | Live |
| Korean | https://about.gitlab.com/ko-kr/ | In progress |
| Italian | https://about.gitlab.com/it-it/ | In progress |
| Portugese | https://about.gitlab.com/pt-br/ | In progress |
| Spanish | https://about.gitlab.com/es-es/ | In progress |
| Russian | https://about.gitlab.com/ru-ru/ | In progress |
