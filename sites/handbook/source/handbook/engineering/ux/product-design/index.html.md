---
layout: handbook-page-toc
title: "Product Design"
description: "We support the business of GitLab by becoming experts in our stage group, educating ourselves about the entire product, and staying engaged with user and business goals"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Design at GitLab

We support the business of GitLab by becoming experts in our stage group, educating ourselves about the entire product, and staying engaged with user and business goals. We partner closely with our stable counterparts in Product Management and Development. 


## Team Structure

Each Product Designer is assigned to an area of our product, called Stage Groups. They learn everything they can about users and their workflows to design solutions for real customer problems. 

Information about product categories and links to team members and direction pages can be found [here](/handbook/product/categories/).

Some UX teams have documented detailed information about their ways of working and stage groups, and these can be found here: 
* [Ops](https://about.gitlab.com/direction/ops/) (@gitlab-com/gitlab-ux/ops-ux)
    * [CI/CD](/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/) (@gitlab-com/gitlab-ux/cicd-ux)
        * [Verify UX](/handbook/engineering/ux/stage-group-ux-strategy/verify/)
        * [Package UX](/handbook/engineering/ux/stage-group-ux-strategy/package/)
        * [Release UX](/handbook/engineering/ux/stage-group-ux-strategy/release/)
    * Configure and Monitor (@gitlab-com/gitlab-ux/configure-monitor-ux)
        * [Configure](https://about.gitlab.com/direction/configure/)
        * [Monitor](https://about.gitlab.com/direction/monitor/)
* [Enablement](/handbook/engineering/ux/stage-group-ux-strategy/enablement/) (@gitlab-com/gitlab-ux/enablement-ux)
* [Sec](https://about.gitlab.com/direction/security/)
    * [Secure and Protect](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/sec/) (@gitlab-com/gitlab-ux/secure-protect-ux)

## Product Design Workflow

Product Designers follow the guidance outlined in the [Product Development flow](/handbook/product-development-flow/) while working on stage group work with our stable counterparts. 

For specific details:
* [Planning and managing capacity](/handbook/engineering/ux/product-designer/#planning-and-managing-capacity)
* [Prioritization](/handbook/engineering/ux/product-designer/#priority-for-UX-issues)
* [Working on Issues](/handbook/engineering/ux/product-designer/#working-on-issues) 
* [Design Process](/handbook/engineering/ux/product-designer/#product-design-process)
* [Partnering with UX Resesarch](/handbook/engineering/ux/product-designer/#product-design-process)
* [Partnering with Technical Writers](/handbook/engineering/ux/product-designer/#partnering-with-technical-writers)
* [Contributing to Pajamas](https://design.gitlab.com/get-started/contribute)

Are you a new GitLab Product Designer? If so, welcome! Make sure you see the [Product Designer Workflow](/handbook/engineering/ux/product-designer/) handbook page that will help you get started.

## Learn about UX and see our work

* [YouTube Playlist for UX Showcases](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz) 
* [UX Learning and Development page](/handbook/engineering/ux/learning-and-development)

## Design Principles

Our [design principles](https://design.gitlab.com/get-started/principles) can be found with the Pajamas Design System. 

## Macro UX

Bringing all of the DevOps stages together in one platform creates an inherently better user experience than using a siloed toolchain. However, we know that we can't stop there. Inside of our product, workflows between product areas must be cohesive and connected, so that users can complete higher-level Jobs to be Done (main jobs).

### Proposal

In Q4 FY22, we will run an experiment that pairs a Staff Engineer with Senior/Staff Product Designer to refine an existing workflow. The Engineer and Product Designer will work together to identify areas of friction in the workflow, and they will resolve them by submitting Merge Requests to the product in real time while pairing. The assigned Engineer will focus on MRs that make larger, more complex product changes, while the Product Designer will contribute smaller CSS changes, documentation, and design expertise. 

The initial topics that teams will focus on during the experiment include:

| Topic                            | Workflow | Engineer | Product Designer |
| -------------------------------- | --------   | -------- | ---------------- |
| Auto DevOps                      | TBD        | TBD      | TBD              |
| Kubernetes agent                 | Run, deploy, manage, and observe a container-based application  | Shinya Maeda | Mike Nichols |
| Install experience               | TBD        | TBD      | TBD              |

### How will we measure success?

- **MRs merged:** Total number of MRs merged with the `Macro UX` label. 
- **Video walkthrough:** At the beginning of the project, the Product Designer will work with the Engineer to create a video walkthrough that focuses on a specific higher-level JTBD and how the current user experience of that workflow supports it. At the end of the project, the Product Designer will create a video walkthrough that shows the improved experience.

### Risks
- We don't know how much time will be required during the experiment for these pairings to be successful, so we can't predict the impact to participants' regular milestone work, OKRs, and so on.
- The experiment will focus on fixing friction points identified during heuristic reviews, which means that we won't conduct user research. There is a possibility that we will inadvertently introduce new friction points.
- The topics identified for the Q4 experiment are not items we hear about frequently in user research, so it's likely that during the experiment we will focus on areas that will not improve our overall product usability score as measured by [SUS](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/#system-usability-scale-sus-score).

