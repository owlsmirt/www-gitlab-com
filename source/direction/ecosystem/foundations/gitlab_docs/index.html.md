---
layout: markdown_page
title: "Category Direction - GitLab Documentation Site"
canonical_path: "/direction/ecosystem/foundations/gitlab_docs/"
---

- TOC
{:toc}

## GitLab Documentation Site

|                       |                               |
| -                     | -                             |
| Stage                 | [Ecosystem](/direction/ecosystem/)      |
| Maturity              | N/A |
| Content Last Reviewed | `2021-10-25`                  |

## Overview

Our goal is to create documentation that is complete, accurate, and easy to use. It should be easy to browse or search for the information you need, and easy to contribute to the documentation itself. All standards and practices for contributing documentation are found within the docs in the [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/) section.

GitLab’s documentation is crafted to help users, admins, and decision-makers learn about GitLab features and to optimally implement and use GitLab to meet their [DevOps](https://about.gitlab.com/stages-devops-lifecycle/) needs. Its source is developed and stored with the product in its respective paths within the GitLab [CE](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc), [EE](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc), [Runner](https://gitlab.com/gitlab-org/gitlab-runner/tree/master/docs), and [Omnibus](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc) repositories. The documentation is published at [docs.gitlab.com](https://docs.gitlab.com/) (offering multiple versions of all products’ documentation) and at the `/help/` path on each GitLab instance’s domain, with content for that instance’s version and edition.

### Target Audience

**GitLab Users:** Users of GitLab rely on accurate, up-to-date, and comprehensive documentation of the features available.

**GitLab Team Members:** GitLab team members are both contributors to and consumers of the documentation. While the [Technical Writing](https://about.gitlab.com/handbook/product/technical-writing/) team owns the documentation, all team members can and do contribute to the them.

**Leadership & Decision-makers:** The GitLab documentation site is a valuable resource for decision-makers to compare features across versions and evaluate implementation details that may impact a purchasing decision.

### Challenges to address

A recent round of UX research highlighted a few key opportunities for improving the GitLab documentation site:

- In general, it can be difficult to find specific information in the documentation 
- The documentation would benefit from more contextual _why_ information
- The `Troubleshooting` information is the among the most difficult content to find
- The difference between `/help` and `docs.gitlab.com` is confusing
- Distinguishing between multiple release versions is challenging

### Where we are Headed

Our current focus is on improving the information architecture, usability, and overall content of the documentation. Specifically, we're working to:

* Complete information architecture and user experience audit
* Complete a gap analysis of the documentation content itself
* Improve search and discoverability across the site

Additionally, we want to lower the barrier to contributing changes by integrating the [static site editor](https://about.gitlab.com/direction/create/editor/static_site_editor/).  


### What's Next & Why

As outlined in the [Documentation Roadmap epic](https://gitlab.com/groups/gitlab-org/-/epics/4602) there are a number of improvements prioritized to address the challenges listed above. 

1. Upcoming [visual design improvements](https://gitlab.com/groups/gitlab-org/-/epics/4527) are intended make the docs site less cluttered, more polished, and easier to scan.
1. Consolidating the `/help` and `docs.gitlab.com` experiences and [making the documentation available offline for airgapped instances](https://gitlab.com/gitlab-org/gitlab/-/issues/214164) will clarify where to look for the relevant documentation.
1. Providing a [defined content strategy](https://gitlab.com/groups/gitlab-org/-/epics/4655) will improve topic organization and improve site findability and usability.
1. Identify ways to provide more [extensive and findable troubleshooting docs](https://gitlab.com/groups/gitlab-org/-/epics/4702).
1. An [improved and consistent information architecture](https://gitlab.com/groups/gitlab-org/-/epics/4654) can help users find what they need more easily.
1. Improvements to [identifying and differentiating between versions](https://gitlab.com/groups/gitlab-org/-/epics/4707) in the documentation.

In addition, we're planning to invest in an [improved search experience](https://gitlab.com/groups/gitlab-org/-/epics/4656), end-to-end tutorial content, and [context-aware documentation](https://gitlab.com/gitlab-org/technical-writing/-/issues/335). 

### What is Not Planned Right Now

At this time, we are not investigating any significant architectural changes to the documentation site itself, like migrating to a new static site generator.

We're not currently investing in localization of the documentation.

### Maturity Plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->


Currently, the GitLab Documentation Site category is a *non-marketing category* which means its maturity does not get tracked.


<!--
### User success metrics
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->


<!--
### Why is this important?
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

* [Stripe's documentation](https://stripe.com/docs) is considered the gold standard of documentation sites
* [Algolia](https://www.algolia.com/doc/) has excellent documentation and information architecture


<!--

### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->
